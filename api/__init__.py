from flask import Flask


app = Flask(__name__)

from api.views.authentication import authentication
app.register_blueprint(authentication, url_prefix='/auth')

from api import app
from decorators.secured import secured
from api.views.hits import hitme
import redis
r = redis.Redis('localhost')
r.set('counter', 0)

app.run(host='0.0.0.0', port=8080, debug=True)

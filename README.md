# A Quick Flask Demo

A quick demo of Flask, with basic hello world, blueprints, nose, mock & patch (testing), and a minimal authentication decorator

Commits are checkpointed to basic features.

## Establishing a virtualenv and dependencies

* Install virtualenv (sudo easy_install virtualenv on mac)
* ```virtualenv ~/Projects/environments/flask-example```
* ```source ~/Projects/environments/flask-example/bin/activate```
* within directory: ```pip install -r requirements.txt```

## Running the server

At any commit, you should be able to run:

```python start.py```

## Running the tests

```nosetests -sxv```
